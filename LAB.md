# Lab

[Timus Online Judge](https://acm.timus.ru/?locale=en)

## Author

Igor Parfenov

Contact: [@Igor_Parfenov](https://t.me/Igor_Parfenov)

## Request Password

| Test step | Result |
|---|---|
| Go to `https://acm.timus.ru/authedit.aspx` | There is *Request password* form |
| Enter existing *Email* into field and click *Request password* | *Message sent.* text appeared. New message in *Email* appeared. There is old password in text of message. ![Request Password](/screenshots/1-1.png) ![Request Password](/screenshots/1-2.png) |
| Enter non-existing *Email* into field and click *Request password* | *No authors registered on this email* text appeared. ![Request Password](/screenshots/1-3.png) |

`Fail`

Passwords are stored as plain text.

[Password Storage](https://cheatsheetseries.owasp.org/cheatsheets/Password_Storage_Cheat_Sheet.html)

User enumeration attack is possible.

[Forgot Password Request](https://cheatsheetseries.owasp.org/cheatsheets/Forgot_Password_Cheat_Sheet.html#forgot-password-request)

## XSS - Cross-Site Scripting

| Test step | Result |
|---|---|
| Go to `https://acm.timus.ru` | There is *Author's name - Search* form |
| Enter `<script>alert(1)</script>` into field and click *Search* | No XSS. Furthermore, there is already a user with name `<script>alert(1)</script>` ![XSS](/screenshots/2.png) |

`Success`

[XSS](https://owasp.org/www-community/attacks/xss/)

## SQL Injection

| Test step | Result |
|---|---|
| Go to `https://acm.timus.ru` | There is *Author's name - Search* form |
| Enter `' OR 1=1 --` into field and click *Search* | Empty search results. ![SQL Injection](/screenshots/3.png) |

`Success`

[SQL Injection](https://owasp.org/www-community/attacks/SQL_Injection)

## Sequential ID

| Test step | Result |
|---|---|
| Go to `https://acm.timus.ru` | There is *Author's name - Search* form |
| Enter `<script>alert(1)</script>` into field and click *Search*. Click on appeared user name. | The *URL* is `https://acm.timus.ru/author.aspx?id=239254` |
| Check date of first submission of the user | The first submission is made on *24.11.2017* ![Sequential ID](/screenshots/4-1.png) |
| Increment `id` in *URL* and find next users with at least one submission (most users have no submissions) and check dates of their first submissions. | The `https://acm.timus.ru/author.aspx?id=239257` has first submission on *14.12.2017*. ![Sequential ID](/screenshots/4-2.png) The `https://acm.timus.ru/author.aspx?id=239262` has first submission on *27.12.2017*.  ![Sequential ID](/screenshots/4-3.png) |

`Fail`

The ID's are given to registered users sequentially.

[Insecure Direct Object Reference Prevention](https://cheatsheetseries.owasp.org/cheatsheets/Insecure_Direct_Object_Reference_Prevention_Cheat_Sheet.html)
